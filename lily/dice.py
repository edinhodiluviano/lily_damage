import re
from typing import List

import pydantic


class Die(pydantic.BaseModel):
    number: int
    value: int

    def mean(self):
        m = self.number * (self.value + 1) / 2
        return m

    @classmethod
    def from_string(cls, string: str):
        patt = r"^([+-]?\d{1,})$"  # 5; +4; -1
        if m := re.match(patt, string):
            number = m.groups()[0]
            number = int(number)
            value = 1
            return Die(number=number, value=value)

        patt = r"^([+-]?\d{0,})d(\d{1,})$"  # d6; 2d8; +3d4; -2d10; -d4
        if m := re.match(patt, string):
            number = m.groups()[0]
            if number in {"", "+"}:
                number = 1
            elif number == "-":
                number = -1
            number = int(number)
            value = m.groups()[1]
            value = int(value)
            return Die(number=number, value=value)

        raise ValueError(f"Die string format ({string}) not recognized")


class Dice(pydantic.BaseModel):
    list: List[Die]

    @classmethod
    def from_string(cls, string: str):
        patt = r"([\+-])"
        splits = re.split(patt, string)
        if splits[0] in {None, ""}:
            splits = splits[1:]
        if splits[0] not in {"+", "-"}:
            splits = ["+"] + splits
        print(splits)
        dice_list = []
        for n in range(0, len(splits), 2):
            signal = splits[n]
            text = splits[n + 1]
            die = Die.from_string(signal + text)
            dice_list.append(die)
        dice_obj = Dice(list=dice_list)
        return dice_obj

    def mean(self):
        return sum([die.mean() for die in self.list])
