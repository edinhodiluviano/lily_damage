import os
import time
import logging
import zipfile
import tempfile
import subprocess
import urllib.parse
from pathlib import Path
from typing import List, Union

import boto3
from botocore.exceptions import ClientError


logger = logging.getLogger(__name__)


class Deploy:
    def __init__(
        self,
        dest_file: str,
        extra_dirs: Union[List[str], str],
        requirements: str = "requirements.txt",
        lambda_function: str = "lambda_function.py",
    ):
        self.dest_file = Path(dest_file)
        if isinstance(extra_dirs, (str, Path)):
            extra_dirs = [extra_dirs]
        self.extra_dirs = [Path(d) for d in extra_dirs]
        self.requirements = Path(requirements)
        self.lambda_function = Path(lambda_function)
        self._check()

    def _check(self):
        self._check_new()
        self._check_existing()

    def _check_new(self):
        if self.dest_file.exists():
            msg = f"Destination file ({self.dest_file}) already exists"
            raise ValueError(msg)

    def _check_existing(self):
        for dir_ in self.extra_dirs:
            if not dir_.exists():
                raise FileNotFoundError(f"Directory ({dir_}) not found")
        if not self.requirements.exists():
            msg = f"Requirements {self.requirements} not found"
            raise FileNotFoundError(msg)
        if not self.lambda_function.exists():
            msg = f"Lambda function ({self.lambda_function}) not found"
            raise FileNotFoundError(msg)

    def create_zip(self):
        logger.info(f"Creating zip file {self.dest_file.name}")
        with tempfile.TemporaryDirectory() as tmpdir:
            tmpdir = Path(tmpdir)
            tmpfilename = tmpdir / "tmpfile"

            with zipfile.ZipFile(
                tmpfilename,
                mode="a",
                compression=zipfile.ZIP_DEFLATED,
                compresslevel=9,
            ) as fzip:

                # add lambda_function
                self._add_one_to_zip(
                    fzip,
                    src=self.lambda_function,
                    zip_path=self.lambda_function.name,
                )

                # add extra dirs
                for dir_ in self.extra_dirs:
                    self._add_dir_to_zip(
                        fzip,
                        src=dir_,
                        zip_root=Path(""),
                    )

                # install packages
                pkg_folder = tmpdir / "packages"
                os.mkdir(pkg_folder)
                pip_install(self.requirements, pkg_folder)

                # add packages
                for node in pkg_folder.glob("*"):
                    node = Path(node)
                    if node.is_file():
                        self._add_one_to_zip(
                            fzip, src=node, zip_path=node.name
                        )
                    else:
                        self._add_dir_to_zip(
                            fzip, src=node, zip_root=pkg_folder
                        )

            os.rename(tmpfilename, self.dest_file)

    def _add_dir_to_zip(self, fzip, src: Path, zip_root: Path):
        logger.debug(f"Adding directory {str(src)}")
        files_all = src.glob("**/*")
        files_filtered = [Path(f) for f in files_all if _keep(f)]
        for file in files_filtered:
            self._add_one_to_zip(fzip, file, file.relative_to(zip_root))

    def _add_one_to_zip(self, fzip, src: Path, zip_path: Path):
        logger.debug(f'Adding file "{str(src)}" as "{str(zip_path)}"')
        zip_filename = str(zip_path)
        zinfo = zipfile.ZipInfo(zip_filename)
        zinfo.external_attr = 0o644 << 16
        zinfo.compress_type = zipfile.ZIP_DEFLATED
        with open(src, "rb") as f:
            fzip.writestr(zinfo, f.read())

    def send_to_s3(self):
        logger.info("Sending to S3")
        s3_path = os.environ["S3_PATH"]
        self.s3_key = s3_path + "/" + self.dest_file.name
        self.s3_url = upload_file(
            file_name=str(self.dest_file),
            bucket=os.environ["S3_BUCKET"],
            object_name=self.s3_key,
        )

    def update_lambda_code(self):
        logger.info("Updating lambda function code")
        client = boto3.client("lambda")
        response = client.update_function_code(
            FunctionName=os.environ["LAMBDA_ARN"],
            S3Bucket=os.environ["S3_BUCKET"],
            S3Key=self.s3_key,
        )
        print(response)


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False

    code from https://boto3.amazonaws.com/v1/documentation/api/latest/guide/s3-uploading-files.html  # NOQA: E501
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3_client = boto3.client("s3")
    try:
        _ = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False

    components = urllib.parse.ParseResult(
        scheme="https",
        netloc=f"{bucket}.s3.amazonaws.com",
        path=urllib.parse.quote(object_name),
        params="",
        query="",
        fragment="",
    )
    url = urllib.parse.urlunparse(components)
    return url


def _keep(file):
    if not file.is_file():
        return False
    filename = str(file)
    if "__pycache__" in filename:
        return False
    if filename.endswith(".swp"):
        return False
    return True


def pip_install(requirements: Path, target: Path):
    cmd = f"pip install --target {target} -r {requirements}"
    logger.debug(f"Running pip install. {cmd=}")
    start = time.time()
    proc = subprocess.run(
        cmd.split(),
        capture_output=True,
        timeout=600,
        check=True,
    )
    end = time.time()
    logger.debug(f"Finished pip install in {end - start:.1f} seconds")
    return proc
