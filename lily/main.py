import datetime as dt

from fastapi import FastAPI


app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/datetime")
async def datetime():
    return {"timestamp": dt.datetime.utcnow().isoformat()}
