import time
import urllib.parse

import requests
from pytest import fixture


URL = "https://rtdg9bq9o7.execute-api.us-east-1.amazonaws.com"


class TestSession(requests.Session):
    def request(self, method, url, *args, **kwargs):
        time.sleep(1.1)
        url = urllib.parse.urljoin(URL, url)
        return super().request(method, url, *args, **kwargs)


@fixture
def client():
    client = TestSession()
    yield client
