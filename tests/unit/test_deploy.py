import os
from pathlib import Path

import pytest

import lily


@pytest.fixture
def zipfolder():
    this_file_folder = Path(os.path.realpath(__file__)).parent
    return this_file_folder / "deploy/zip"


def test_deploy_check_new(tmpdir, zipfolder):
    with open(tmpdir / "a", "w") as f:
        f.write("a")
    with pytest.raises(ValueError):
        _ = lily.deploy.Deploy(
            dest_file=tmpdir / "a",
            extra_dirs=zipfolder / "lily",
            requirements=zipfolder / "requirements.txt",
            lambda_function=zipfolder / "lambda_function.py",
        )


def test_deploy_check_existing_dir(tmpdir, zipfolder):
    with pytest.raises(FileNotFoundError):
        _ = lily.deploy.Deploy(
            dest_file=tmpdir / "created.zip",
            extra_dirs=zipfolder / "aaaaaaaaaaaaaaj",
            requirements=zipfolder / "requirements.txt",
            lambda_function=zipfolder / "lambda_function.py",
        )


def test_deploy_check_existing_requirements(tmpdir, zipfolder):
    with pytest.raises(FileNotFoundError):
        _ = lily.deploy.Deploy(
            dest_file=tmpdir / "created.zip",
            extra_dirs=zipfolder / "lily",
            requirements=zipfolder / "aaaaaaaaaaaaaaa",
            lambda_function=zipfolder / "lambda_function.py",
        )


def test_deploy_check_existing_lambda_function(tmpdir, zipfolder):
    with pytest.raises(FileNotFoundError):
        _ = lily.deploy.Deploy(
            dest_file=tmpdir / "created.zip",
            extra_dirs=zipfolder / "lily",
            requirements=zipfolder / "requirements.txt",
            lambda_function=zipfolder / "aaaaaaaaaaaaaaaaa",
        )
