import datetime as dt


def test_should_always_pass():
    return


def test_root_returns_200(client):
    resp = client.get("")
    assert resp.status_code == 200


def test_slash_returns_200(client):
    resp = client.get("/")
    assert resp.status_code == 200


def test_bogus_returns_404(client):
    resp = client.get("lsakdjflaksjdfs")
    assert resp.status_code == 404


def test_docs_returns_200(client):
    resp = client.get("docs")
    assert resp.status_code == 200


def test_open_api_returns_200(client):
    resp = client.get("openapi.json")
    assert resp.status_code == 200


def test_open_api_returns_json_content(client):
    resp = client.get("openapi.json")
    d = resp.json()
    assert isinstance(d, dict)


def test_datetime_returns_200(client):
    resp = client.get("datetime")
    assert resp.status_code == 200


def test_datetime_dict_with_time(client):
    resp = client.get("datetime")
    d = resp.json()
    assert "timestamp" in d
    assert dt.datetime.fromisoformat(d["timestamp"])
