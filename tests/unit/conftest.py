from pytest import fixture

from fastapi.testclient import TestClient

import lily


@fixture
def client():
    yield TestClient(lily.app)
