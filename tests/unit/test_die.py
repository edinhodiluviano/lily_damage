import pytest

from lily.dice import Die, Dice


def test_should_always_pass():
    pass


die_from_string_cases = [
    ("0", Die(number=0, value=1)),
    ("1", Die(number=1, value=1)),
    ("+1", Die(number=1, value=1)),
    ("-1", Die(number=-1, value=1, positive=False)),
    ("d4", Die(number=1, value=4)),
    ("d6", Die(number=1, value=6)),
    ("1d6", Die(number=1, value=6)),
    ("2d6", Die(number=2, value=6)),
    ("10d10", Die(number=10, value=10)),
    ("+d4", Die(number=1, value=4)),
    ("+6d4", Die(number=6, value=4)),
    ("-d4", Die(number=-1, value=4)),
    ("-1d4", Die(number=-1, value=4)),
    ("-6d4", Die(number=-6, value=4)),
]


@pytest.mark.parametrize("given,expected", die_from_string_cases)
def test_die_from_string_cases_ok(given, expected):
    assert Die.from_string(given) == expected


die_from_string_incorrect_formats = [
    "a", "d6a", "b6", "a6", "1a6", "-a", "++d6", "1.5d6",  # NOQA: BLK100
    "d2.5", "10d"  # NOQA: BLK100
]


@pytest.mark.parametrize("given", die_from_string_incorrect_formats)
def test_die_from_string_cases_incorrect_formats(given):
    with pytest.raises(ValueError):
        _ = Die.from_string(given)


die_means_cases = [
    ("d4", 2.5),
    ("d6", 3.5),
    ("1d4", 2.5),
    ("2d6", 7.0),
    ("-d4", -2.5),
    ("-2d4", -5.0),
    ("10d10", 55.0),
    ("5", 5.),
    ("+5", 5.),
    ("-5", -5.),
]


@pytest.mark.parametrize("given,expected", die_means_cases)
def test_die_means(given, expected):
    die = Die.from_string(given)
    assert die.mean() == expected


dice_from_string_cases = [
    ("1d6", [Die(number=1, value=6)]),
    ("d6", [Die(number=1, value=6)]),
    ("+1d6", [Die(number=1, value=6)]),
    ("+d6", [Die(number=1, value=6)]),
    ("2d6", [Die(number=2, value=6)]),
    ("10d60", [Die(number=10, value=60)]),
    ("1d6+1d8", [Die(number=1, value=6), Die(number=1, value=8)]),
    ("1d6+2d8", [Die(number=1, value=6), Die(number=2, value=8)]),
    ("1d6+5", [Die(number=1, value=6), Die(number=5, value=1)]),
    ("1d6+d8", [Die(number=1, value=6), Die(number=1, value=8)]),
    ("1d6+d8+5", [
        Die(number=1, value=6),
        Die(number=1, value=8),
        Die(number=5, value=1)
    ]),
    ("-d6-d8", [Die(number=-1, value=6), Die(number=-1, value=8)]),
    ("-d6+d8+5-d10", [
        Die(number=-1, value=6),
        Die(number=1, value=8),
        Die(number=5, value=1),
        Die(number=-1, value=10),
    ]),
]


@pytest.mark.parametrize("given,expected", dice_from_string_cases)
def test_dice_from_string(given, expected):
    dice = Dice.from_string(given)
    assert dice.list == expected


dice_mean_cases = [
    ("1d6", 3.5),
    ("d6", 3.5),
    ("+1d6", 3.5),
    ("+d6", 3.5),
    ("2d6", 7.0),
    ("10d60", 305.),
    ("1d6+1d8", 8.0),
    ("1d6+2d8", 12.5),
    ("1d6+5", 8.5),
    ("1d6+d8", 8.0),
    ("1d6+d8+5", 13.0),
    ("-d6-d8", -8.0),
    ("-d6+d8+5-d10", 0.5),
]


@pytest.mark.parametrize("given,expected", dice_mean_cases)
def test_dice_mean(given, expected):
    dice = Dice.from_string(given)
    assert dice.mean() == expected
