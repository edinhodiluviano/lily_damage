import json

import lambda_function


def fake_event(endpoint: str):
    d = {
        "headers": {
            "accept": "application/json",
            "accept-encoding": "gzip, deflate, br",
        },
        "requestContext": {
            "http": {
                "method": "GET",
                "path": endpoint,
                "sourceIp": "179.152.256.256",
                "userAgent": (
                    "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) "
                    "Gecko/20100101 Firefox/82.0",
                ),
            },
        },
    }
    return d


def request(endpoint: str, raw: bool = False):
    event = fake_event(endpoint)
    response = lambda_function.lambda_handler(event, {})
    if not raw:
        response = json.loads(response["body"])
    return response


def test_raw_response():
    resp = request("/", raw=True)
    assert isinstance(resp, dict)
    assert resp == {
        "body": '{"message":"Hello World"}',
        "headers": {
            "content-length": "25",
            "content-type": "application/json",
        },
        "isBase64Encoded": False,
        "statusCode": 200,
    }


def test_response_body():
    resp = request("/")
    assert isinstance(resp, dict)
    assert resp == {"message": "Hello World"}
