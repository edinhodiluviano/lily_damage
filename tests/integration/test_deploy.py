import os
import filecmp
from pathlib import Path

import pytest

import lily


@pytest.fixture
def zipfolder():
    this_file_folder = Path(os.path.realpath(__file__)).parent
    return this_file_folder / "deploy/zip"


def test_deploy_zip(tmpdir, zipfolder):
    deploy = lily.deploy.Deploy(
        dest_file=tmpdir / "created.zip",
        extra_dirs=zipfolder / "lily",
        requirements=zipfolder / "requirements.txt",
        lambda_function=zipfolder / "lambda_function.py",
    )
    deploy.create_zip()
    assert filecmp.cmp(tmpdir / "created.zip", zipfolder / "expected.zip")
    assert filecmp.cmp(
        tmpdir / "created.zip",
        zipfolder / "expected.zip",
        shallow=False,
    )
