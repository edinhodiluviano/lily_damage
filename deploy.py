#!/usr/bin/env python3


import os
import datetime as dt
import logging.config

import lily


logging.config.fileConfig("logging.conf", disable_existing_loggers=False)


def main():
    now = dt.datetime.utcnow().isoformat(timespec="seconds")
    filename = os.environ["LAMBDA_NAME"] + "_" + now + ".zip"

    deploy = lily.deploy.Deploy(
        dest_file=filename,
        extra_dirs=["lily"],
    )
    deploy.create_zip()
    deploy.send_to_s3()
    deploy.update_lambda_code()


if __name__ == "__main__":
    main()
